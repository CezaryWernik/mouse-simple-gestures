/*
MouseGestures - pure Java library for recognition and processing mouse gestures.
Copyright (C) 2003-2004 Smardec

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package com.smardec.mousegestures.test;

import com.smardec.mousegestures.MouseGestures;
import com.smardec.mousegestures.MouseGesturesListener;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

@SuppressWarnings("serial")
public class TestFrame extends JFrame {
    private MouseGestures mouseGestures = new MouseGestures();
    private JLabel statusLabel = new JLabel("Wykonany gest: ");
    private static StoperObj stoper = new StoperObj();
    private static Thread threader = new Thread(stoper);
    private TextArea middleTimeLabel = new TextArea("Czasy po�rednie: \n");
    private InstructionJpg instr;
    
    @SuppressWarnings("deprecation")
	public static void main(String[] args) {
        TestFrame frame = new TestFrame();
        frame.show();
        threader.start();
    }

    public TestFrame() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Stoper na gesty myszy");
        initSize();
        getContentPane().setLayout(new BorderLayout());
        initControls();
        initStatusBar();
        initMouseGestures();
    }

    private void initSize() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension size = new Dimension(640, 480);
        if (size.height > screenSize.height) size.height = screenSize.height;
        if (size.width > screenSize.width) size.width = screenSize.width;
        setSize(size);
        setLocation((screenSize.width - size.width) / 2, (screenSize.height - size.height) / 2);
    }

    private void initControls() {
    	
        JCheckBox jCheckBoxButton1 = new JCheckBox("Prawy przycisk");
        jCheckBoxButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mouseGestures.setMouseButton(MouseEvent.BUTTON3_MASK);
            }
        });
        JCheckBox jCheckBoxButton2 = new JCheckBox("Rolka");
        jCheckBoxButton2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mouseGestures.setMouseButton(MouseEvent.BUTTON2_MASK);
            }
        });
        JCheckBox jCheckBoxButton3 = new JCheckBox("Lewy przycisk");
        jCheckBoxButton3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mouseGestures.setMouseButton(MouseEvent.BUTTON1_MASK);
            }
        });
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(jCheckBoxButton1);
        buttonGroup.add(jCheckBoxButton2);
        buttonGroup.add(jCheckBoxButton3);
        jCheckBoxButton1.setSelected(true);

        JPanel jPanelGeneral = new JPanel(new GridLayout(2, 1));
        
        JPanel jPanelActivatorsAndStoper = new JPanel(new GridLayout(1,2));
        
        JPanel jPanelButtons = new JPanel(new GridLayout(8, 1));
        jPanelButtons.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
        jPanelButtons.add(new JLabel("Wyb�r przycisku uaktywniaj�cego gest myszy:"));
        jPanelButtons.add(jCheckBoxButton1);
        jPanelButtons.add(jCheckBoxButton2);
        jPanelButtons.add(jCheckBoxButton3);
        
        JPanel jPanelStoper = new JPanel(new GridLayout(1, 2));
        jPanelStoper.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
      
        JPanel jPanelForSoperAndLabel = new JPanel();
        jPanelForSoperAndLabel.add(new JLabel("Stoper:"));
        jPanelForSoperAndLabel.add(stoper);
        
        jPanelStoper.add(jPanelForSoperAndLabel);
        jPanelStoper.add(middleTimeLabel);
        
      
        instr = new InstructionJpg();
        instr.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
        instr.setBorder(BorderFactory.createLoweredBevelBorder());
        
        jPanelActivatorsAndStoper.add(jPanelButtons);
        jPanelActivatorsAndStoper.add(jPanelStoper);
        
        jPanelGeneral.add(jPanelActivatorsAndStoper);
        jPanelGeneral.add(instr);

        getContentPane().add(jPanelGeneral, BorderLayout.NORTH);
        
    }

    private void initStatusBar() {
        JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jPanel.setBorder(BorderFactory.createLoweredBevelBorder());
        jPanel.add(statusLabel);
        getContentPane().add(jPanel, BorderLayout.SOUTH);
    }

    private void initMouseGestures() {
        mouseGestures = new MouseGestures();
        mouseGestures.addMouseGesturesListener(new MouseGesturesListener() {
            public void gestureMovementRecognized(String currentGesture) {
                setGestureString(addCommas(currentGesture));
            }

            public void processGesture(String gesture) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {}
                setGestureString("");
            }

            private String addCommas(String gesture) {
                StringBuffer stringBuffer = new StringBuffer();
                for (int i = 0; i < gesture.length(); i++) {
                    stringBuffer.append(gesture.charAt(i));
                    if (i != gesture.length() - 1)
                        stringBuffer.append(",");
                }
                return stringBuffer.toString();
            }
        });
        mouseGestures.start();
    }

    private void setGestureString(String gesture) {
        statusLabel.setText("Wykonany gest: " + gesture);
        switch(gesture){
        	case"R":{
        		middleTimeLabel.append(stoper.getText()+"\n");
        	}break;
        	case"L,R":{//
        		middleTimeLabel.setText("Czasy po�rednie: \n");
        	}break;
        	case"L,D,R,D,L":{//Set
        		stoper.Set();
        	}break;
        	case"U,R,D,L,D":{//Reset
        		stoper.Reset();
        	}break;
        }
    }
}
