package com.smardec.mousegestures.test;
import javax.swing.*;


@SuppressWarnings("serial")
public class StoperObj extends JLabel implements Runnable{
	protected boolean runing;
	protected int h,m,s,set;
	
	public StoperObj(){
		this.Reset();
	}
	
	public void Set(){
		if(runing){
			this.Stop();
		}else this.Start();
	}
	
	protected void Start(){
		runing = true;
	}
	
	public void Reset(){
		h=0;
		m=0;
		s=0;
		set=0;
		runing = false;
	}
	
	protected void Stop(){
		runing = false;
	}
	
	protected String conv( int h2, int m2, int s2, int set2){
		StringBuilder result = new StringBuilder("");
		result.append(((h2<10)?("0"+h2):(h2))+":");
		result.append(((m2<10)?("0"+m2):(m2))+":");
		result.append(((s2<10)?("0"+s2):(s2))+":"+((set2<10)?("00"+set2):(((set2<100)?("0"+set2):(set2)))));
		
		return result.toString();
	}

	@Override
	public void run() {
		while(true) {
            try {
                Thread.sleep(1);
                if(runing==true){
                	set++;
                	if(set>=1000){
                		s++;
                		set=0;
                	}
                	
                	if(s>=60){
                		m++;
                		s=0;
                	}
                	
                	if(m>=60){
                		h++;
                		m=0;
                	}
                }
                this.setText(conv(h, m, s, set));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
	}
}
